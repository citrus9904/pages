function setTheme(theme) {
	const remove = (theme === "light")?"dark":"light";
	const el = document.querySelector("html")?.classList;
	el.add(theme);
	el.remove(remove);
}

function toggleTheme() {
	const el = document.querySelector("html")?.classList;
	let theme = "light";
	if (el.contains("light")) {
		theme = "dark";
	}
	setTheme(theme);
	localStorage.setItem("theme", theme);
}

document.getElementById("toggler-button").addEventListener("click", toggleTheme);

// manual toggle assumes high priority
if (localStorage.getItem("theme")) {
	setTheme(localStorage.getItem("theme"));
} else {
	window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', event => {
		const newColorScheme = event.matches ? "dark" : "light";
		setTheme(newColorScheme);
	});

	if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
		setTheme("dark");
	}
}
