Meditation has become an integral part of my life. I feel better about 20 percent better when I regularly meditate. However, the primary reason one must meditate is not so that they feel better. Bear with me, but here is the true reason why one should meditate: to observe the nature of the human mind. When I sit down to meditate, I am always amazed at how much the human mind does, all on its own—thoughts and emotions continually arise, without any seeming participation of myself. Even more bizzarely, they pass away within moments, making me feel a whole different way. Observing this phenomenon is truly powerful. It reveals some experiential truths about the human mind. I find this fascinating.

In this section of my corner of the internet, I intend to share my progress and struggles I face in my meditative practice. I mainly engage in two kinds of meditation: [Vipassana](https://en.wikipedia.org/wiki/Vipassanā) and focus meditation. Both these meditation techniques do not require a belief in one or the other religion—both are non-sectarian, and anybody can take them up.

Vipassana is a technique that encourages clear observation of the human mind—please note that I do not mean this in an abstract sense: Vipassana literally entails clear observation of the human mind. Being able to observe what the mind does, as I have stated before, is powerful, bizarre, and fascinating to the utmost degree. The expert Vipassana practitioner also observes their own conception of the "self", and experiences selflessness (this can also be induced more reliably by psychedelics!), an experience which allows one to become less conscious of themself, and more compassionate and solicitous toward others.

Vipassana must be complemented with focus meditation. Otherwise, the technique is very difficult to practice and master—at least in my experience. Moreover, focus meditation provides a dynamo of tangible real-life benefits, such as, you guessed it, greater focus. I divide focus meditation into two types: experiential and dialogical. In the dialogical method, you focus on a dialogical object of consciousness, such as a thought or a voice in your head (when you repeatedly count a set of numbers, for instance). Experiential focus involves focus on a specific sensation—such as breathing. I have come to prefer focus meditation grounded in experiential focus, because there is already too much dialogical substance in the human mind anyway!

I have Sam Harris to thank for getting me on the meditative path. If you have not already, check out his [Waking Up](https://www.wakingup.com/) app, which has a phenomenal introductory meditation course.

Here is a tracker of the time I spent meditating, since September 26, 2022, for my own reference (you know, the whole 10000 hours deal):


| Meditation Type | Time Spent |
| ---------------------- | --------------- |
| Focus Meditation | 330 minutes |
| Vipassana | 250 minutes |

Here are some of my writings describing my journey:

* October 3, 2022 [My Current Struggles with Focus Meditation](meditative-practice/struggles_with_focus_meditation.html)
* September 27, 2022 [Vipassana or Focus Meditation?](meditative-practice/vipassana_or_focus_meditation.html)
