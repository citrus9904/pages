# The Meaning Affliction

**October 25, 2022**

Long before COVID-19, another scourge had already managed to afflict almost
everybody on earth. The scourge I am referring to is meaninglessness. Its
symptoms are fairly manifest—excessive watching of the television,
doom-scrolling through Youtube and Facebook, and a seemingly unbreakable
attachment to the modern smartphone. In effect, we have had short-form content
on Youtube and Facebook substituted for meaning. While its long-term efficacy
in providing meaning are but doubtful, their short-term efficacy can hardly be
disputed. Do you have nothing to do? No problem: just hop on to Youtube, and
watch a couple of funny videos—that is sure to make you feel better. Or better
yet, why not log in to Facebook, find an old acquaintance you would not have
otherwise found, and claim that that is precisely why you use Facebook in the
first place? After all, would not this newly refreshed acquaintance help you
expand your network and give you a better footing in the world? (Cal Newport
talks about this kind of logic at length in his book titled "Digital
Minimalism".)

When did meaninglessness at such a large scale first begin? In this essay, I
will contend that it began with the disintegration of traditional familial and
tribal structures when the industrial age dawned. To support my contention, I
will be borrowing heavily from Theodore Kaczynski's seminal manifesto,
Industrial Society and Its Future (whose conclusion is disagree with). Just
before the industrial era, when people found time to idle, they spent time in
their respective communities, mostly engaging in conversation or playing games
and sports such as horse racing—albeit this was likely exclusive to the gentry.
Women spent not little time knitting together or engaging in handicraft in a
common room, or taking long walks through parks. Book reading and intellectual
activity was also highly encouraged and appreciated after the advent of the
printing press. The common theme was people's spending wholesome time together,
which likely gave them meaning and something to look forward to.

However, with the atomization of tribes and families post the industrial era, a
source of meaning was essentially banished. What instead remained to siphon
meaning from was impersonal and mechanical jobs—which are manifestly inferior
to the sources of meaning delineated in the foregoing paragraph. However, a
solution to this problem arose in the west—intellectual activity. Engaging in
scientific pursuits, for example, is a massive source of meaning for many,
because it helps usher the world forward. Meaning typically comes when you
submit to something larger than yourself or anyone else, and science arguably
provides the largest contrast when compared to all other sources of meaning.
For instance, if you are a scientist working working on solar panel technology,
you essentially have a silo of meaning to derive happiness from.

Not all places have been equally fortunate to find an antidote to the meaning
problem, however. While the west has been prosperous for a long time by this
point, places like India have not. Naturally, a different route was taken to
derive meaning: supporting one's family became the highest good. Working,
regardless of whether or not you enjoyed work (or whether or not the work
itself had any intrinsic meaning), was the antithesis of sin, and the ideal
everyone must strive toward. With time, however, people do get affluent enough
to drop their prior definition of what meaning is—but they often do not. They
continue to work on something they probably do not like doing, because that is
what the society has come to value. This ties into the commonly observed
phenomenon of people's being surprised to see one, say, learning something
interesting, on a weekend. "Isn't it a holiday for you? Why are you working?",
they might ask. In effect, a "job" is where you work at. There seems to be no
concept of intellectual growth and creativity outside of work.

This focus on work and the annihilation of long-standing familial and tribal
structures has wreaked havoc on society. Now that work for the sake of work is
prized, spending time together means little—this is clearly perceptible in how
people today spend their "together time": even when they are together, they are
hardly together; they are instead drawn in by their phones. This is a natural
consequence of our shifting of the locus of meaning. I argue that this pretense
has to stop. Why people spend time together in the first place is a remnant of
our prior values and, more importantly, a biological need. If it is done in a
perfunctory manner, it is far worse that its being done at all. Therefore, let
us bring back our prior values or engage in deriving meaning in a wholesome
way, for unwholesome derivation of meaning is worse than no meaning at all.
Unwholesome derivation of meaning is what leads to the seeking of short-term
pleasures such as constantly watching short-form videos on Youtube.

Funnily enough, I might have made it seem as though I have a personal vendetta
against Youtube in particular! Reader, please rest assured, that I do not. In
fact, Youtube is an ocean of utility and growth—as long as—bear with the
incoming platitude—it is used mindfully and with control. Perhaps set a fixed
time during the day you will browse through Youtube or talk to your friends on
Facebook—you already know what is prudent: everybody does; many just don't
follow their innate wisdom.
