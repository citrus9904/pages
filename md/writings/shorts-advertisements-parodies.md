# Shorts: Are TV Advertisements Parodies?

**July 27, 2022**

TV advertisements, to my eye, are becoming stranger by the day. I find it
increasingly difficult to believe that they are not parodies—a form of practical
joke being played on the public at large. I will use
[this](https://youtube.com/watch?v=zIQTwukIDyg) Amul advertisement to make my
point. Going through 30 seconds of this advertisement is a feat in itself.

First, the entire setting of the advertisement is a white room. Next, they
somehow make a transition from their studious and playful kids to the growth of
India as a whole: they also relate this to India's drinking Amul milk. All this
plays out while the actors make embarrassing expressions at the camera. Then the
advertisement takes us to the "benefits" Amul milk confers: health-creation and
setting up of conditions for success. Although that could be true for other milk
brands as well, so the differentiating factor still remains a mystery. All the
while, the advertisement rapidly switches between random actors pretending to
play different sports, with painfully cringe-worthy facial contortions, and
music in the backdrop.

Is this propaganda in plain sight, or is it a parody? I do not know!

Advertisements found on podcasts are usually far better, and most importantly,
reasonable. If you listen to one of the advertisements on the Lex Fridman
Podcast, you will find that it is a rational listing of the benefits of the
product being advertised, what differentiates it from other similar products,
and other information of rational nature. No music (mostly), no facial
contortions, and (apparently) no propaganda. I wish all advertisements were made
that way.
