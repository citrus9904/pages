# Shorts: Setting Up the Right Environment for Expertise

**July 29, 2022**

Improving in a field is arduous only when you are trying to change the status
quo. If you want to achieve expertise without the self-perception of hard work
and with a higher probability of success, you must put yourself in an
environment where being an expert in your chosen field is the status quo—the
norm. Once you do that, there is no other option but to get better: you will
follow processes that gradually (and naturally) take you closer to expertise,
but there will be no exertion on your end. All that saved energy can be used
somewhere else. The realization of what happened will only dawn on you when you
step out of that environment.

Build or place yourself in conditions that allow for effortless pursual of
processes that beget skill and expertise.
