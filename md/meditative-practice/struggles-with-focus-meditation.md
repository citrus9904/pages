# My Current Struggles with Focus Meditation

**October 3, 2022**

I am finding focus meditation quite difficult of late. I hope the difficulties I am facing are merely the ones one encounters on the path to mastery, be it any field.

I initially began my practice with the technique of counting numbers in my head. I suppose it worked well—I was focused to a moderate degree throughout each session.

However, yesterday, I switched to focusing on my breath, because counting numbers felt too dialogical. Focusing on the breath, on the other hand, is more experiential.

This experiential nature of focusing on the breath lead to some problems. Not maintaining a consistent and controlled dialogical thread in the head without doubt invites arbitrary dialogical threads of thought! One has to constantly reposition the "spotlight" on the breath—I suppose that is how it helps improve focus.

Counting numbers allows far less space for other threads of thought. The dialogical "needs" of our monkey minds are already met!

To focus better on my breath, I then began tracking my breaths with . . . numbers. However, I did not like the experience this produced. I was not experiencing fully, nor were the dialogical threads clear enough. This technique, I think, figuratively sits on the fence—I do not like it!

I am left with two options:

* to go back to the dialogical method
* to continue refining my ability to focus on my breath, and improve my experiential focus

I suppose I will alternate between the two techniques. However, I still believe the latter to be a superior form of focus meditation—mainly because it is far harder, and requires an astonishing amount of focus.
