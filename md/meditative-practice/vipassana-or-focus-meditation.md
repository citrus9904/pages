# Vipassana or Focus Meditation?

**September 27, 2022**

Yesterday, I began practicing focus meditation again, after weeks of consistent Vipassana practice. My rationale was the following: in Vipassana, as you become more mindful of your experience, your attention gets increasingly diffuse—at least until you reach the point of "selflessness". This diffusing of attention left me, during most days, in a state where I could not focus on any task at hand. I, therefore, need to train my power of focus.

I am beginning to wonder if there is any point to practicing Vipassana without any real mastery of attention. My best guess is that it is not. I have therefore decided to complement my Vipassana practice with focus meditation.

During my focus meditation sessions yesterday and today, I initially made an effort to focus on my breath, but that did not work out. First, my noticing of the breath seems to invariably lead to my controlling it. Second, when I try to focus on the breath, I become aware of sounds in the environment. I do not think this is conducive to focus meditation—I believe that there must be one object of focus. I repeatedly counted numbers up to ten in my mind instead—in Spanish! Counting numbers seems to work well for me. I visualize each number twice, separated by a comma. Each row thus formed looks like a Python tuple, albeit without the parentheses. Upon reaching ten rows, I begin again. When I do this, sounds and sensations do not make themselves aware in my experience, for the most part. Thoughts do, however, and maintaining focus becomes the primary task. This has been astonishingly better than focusing on the breath, because when I focus on the breath, not only do thoughts appear spontaneously, but so too do sounds and sensations in the body. That would be ideal for Vipassana, but certainly not for focus meditation.

I have decided to add to my website a table of my time spent in meditative practice—with separate columns for Vipassana and focus meditation. I believe it will help me keep track of my progress—I am not sure if this is antithetical to meditative practice, but it is something I want to do.
