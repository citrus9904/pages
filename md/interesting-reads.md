### Computer Science

* [SOLID design principles](https://youtube.com/watch?v=rtmFCcjEgEw)
Insightful talk with great examples for each of the SOLID principles.
* [How to Write Your Computer Science Statement of Purpose](https://writeivy.com/how-to-write-your-computer-science-statement-of-purpose/)

### Articles and Papers

#### Go

* [Errors are values](https://go.dev/blog/errors-are-values)

  For those who want to understand Go's error handling better.

* [Uber Go Style Guide](https://github.com/uber-go/guide/blob/master/style.md)

  "Style" is already taken care of by `gofmt`. This document is all about how
  you can write effective Go.

* [The Go Memory Model](https://go.dev/ref/mem)

  This is a great resource to learn about many synchronization patterns (and
  anti-patterns) in Go.

* [An Introduction to Handlers and Servemuxes in Go](https://www.alexedwards.net/blog/an-introduction-to-handlers-and-servemuxes-in-go)

  This article explains how HTTP requests are typically handled in Go. It is
  incredibly well-written.

#### Philosophy

* [How episodic TV shows may ruin you (Luke
  Smith)](https://youtube.com/watch?v=4bzLzyKnLzc)
* [Hedonism, Asceticism and the Hermetic
  Answer](https://lukesmith.xyz/c/hedonism-asceticism-and-the-hermetic-answer)

  Luke's take against asceticism in this blog post is interesting.

* [Industrial Society and Its Future](https://web.cs.ucdavis.edu/~rogaway/classes/188/materials/Industrial%20Society%20and%20Its%20Future.pdf)

  If you ever want to read a bleak prognosis of our species, Industrial Society
  and Its Future will fit your needs perfectly. While I like the author's
  pessimism little, I believe he brings up valid points that we will have to
  contend with in the future.

#### Miscellaneous

* [Optimize for Happiness](https://tom.preston-werner.com/2010/10/18/optimize-for-happiness.html)

  Tom Preston-Werner, a co-founder of Github, gives advice on how you want to think about your product and build a business around it, at a time when getting venture capital is all the rage, so to speak.

* [Write maintainable code the first
  time](https://www.xplato.dev/writing/write-maintainable-code-the-first-time)

  In this immersive article, Tristan carefully deconstructs the difference
  between easy-to-read code and easy-to-maintain code.
* [The Internet: An Extension of
  Google](https://www.xplato.dev/writing/the-internet-an-extension-of-google)

  An "SEO guy" bashes SEO.

### Podcasts

* [Making Sense #269—Deep Time](https://www.samharris.org/podcasts/making-sense-episodes/269-deep-time)
* [Making Sense #291—Where Is Happiness?](https://www.samharris.org/podcasts/making-sense-episodes/291-where-is-happiness)

### Videos

* [Digital Minimalism: Choosing a Focused Life in a Noisy World, with Professor
  Cal Newport](https://youtube.com/watch?v=gJ0MV3ixnOM)

  He speaks about the "boredom drive" from 48:05 to 50:00. Historically, boredom
  used to drive us toward doing difficult, but rewarding, things. Today, as
  boredom can be dispelled easily through use of the phone, for example, we seem
  to have lost the yearning to put effort into arduous but worthwhile activities.


### Books

* **Homo Deus** by Yuval Noah Harari

  This book poses important questions about the future of humankind in the face
  of advancement of AI and biotechnology. Strangely enough, this work is highly
  reminiscent of *Industrial Society and Its Future*.

* **Privacy is Power** by Carissa Veliéz

  The title of the book is strangely hackneyed. The content, however, is not.
  In the first chapter, Carissa takes us through a day in the life of a man in a
  fictitious world where modern technology scourges him without his knowing the
  cause of his misery. I found the allegory very powerful. Later in the book,
  the author  proposes several practical legislative ideas to tackle privacy
  invasions. Overall, the book is insightful.

* **The Brothers Karamazov** by Fyodor Dostoevsky

  The Brothers Karamazov poses deep questions about our existence and its
  meaning through the use of characters that undergo profound trauma, moral
  suffering, despair, and struggles against their own worse selves. Dostoevsky
  did not live long enough to complete the two-part story: the second book was
  to continue Alyosha's—Dostoevsky's 'hero'—journey. The first part itself was
  exceedingly profound; I wonder what the second part would have been like had
  Dostoevsky lived longer.
