On patience, from Four Thousand Weeks by Oliver Burkeman:
> . . . more often than not, originality lies on the far side of unoriginality.
> The Finnish American photographer Arno Minkkinen dramatises this deep truth
> about the power of patience with a parable about Helsinki's main bus station.
> There are two dozen platforms there, he explains, with several different bus
> lines departing from each one — and for the first part of its journey, each
> bus leaving from any given platform takes the same route through the city as
> all the others, making identical stops. Think of each stop as representing
> one year of your career, Minkkinen advises photography students. You pick an
> artistic direction — perhaps you start working on platinum prints of nudes —
> and you begin to accumulate a portfolio of work. Three years (or bus stops)
> later, you proudly present it to the owner of a gallery. But you're dismayed
> to be told that your pictures aren't as original as you thought, because they
> look like knock-offs of the work of the photographer Irving Penn; Penn's bus,
> it turns out, had been on the same route as yours. Annoyed at yourself for
> having wasted three years following somebody else's path, you jump off that
> bus, hail a taxi, and return to where you started at the bus station. This
> time, you board a different bus, choosing a different genre of photography in
> which to specialise. But a few bus stops later, the same thing happens:
> you're informed that your new body of work seems derivate, too. Back you go
> to the bus station. But the pattern keeps on repeating: nothing you produce
> ever gets recognised as being truly your own.
>
> What's the solution? 'It's simple,' Minkkinen says. 'Stay on the bus. Stay on
> the fucking bus.' A little further out on their journeys through the city,
> Helsinki's bus routes diverge, plunging off to unique destinations as they
> head through the suburbs and into the countryside beyond. That's where the
> distinctive work begins. But it begins at all only for those who can muster
> the patience to immerse themselves in the earlier stage — the trial-and-error
> phase of copying others, learning new skills and accumulating experience.

An [interesting musing](https://twitter.com/DRMacIver/status/1586850511726206983) on society:

> The modern condition is mostly trying to do things on your own that people
> have historically achieved with a large support network and wondering why
> you're tired all the time.


Sam Harris on boredom:

> It is true we encounter boredom less and  less, with all of our gadgets and
> the totality of human knowledge and artistic output available to us. You can
> always listen to your favorite song, or watch a great film, or read a great
> book, or text your friend because you can do these things with a device you
> have by your side 24 hours a day. You might successfully avoid boredom for
> the rest of your life. But you might also not discover what is on the other
> side of boredom. And you might not be aware of the price you pay for being
> compelled to distract yourself, for having lost, or simply never acquired, a
> capacity for doing nothing—a productive capacity, for doing nothing. Once you
> learn to meditate, you realize that boredom is simply a failure to pay
> attention. If something as simple and repetitive as breathing can become a
> source of blissful contemplation—and it can—and if the feeling of boredom
> itself can become an object of intense interest—and it can—there is no way to
> be bored, if you're paying close attention to your experience. So, training
> in meditation is the true cure to boredom. It is a kind of permanent
> inoculation—once you learn to meditate, you will never be truly bored again.
> Now this isn't to say that you won't make choices in life: some activities
> might still seem like a waste of time—and they might be a waste of time,
> given the other things you could do. So you might still walk out of a movie
> or stop reading a book because it is "boring". But when you are left alone
> with yourself, how do you feel? Are you desperate to be distracted by some
> stimulus? Or can you find an intrinsic feeling of well-being, as an intrinsic
> property of just being conscious? The gulf between these two conditions is
> enormous. And in my experience, only meditation allows us to reliably span
> it. 

Amishi Jha (from an article in [The Guardian](https://www.theguardian.com/lifeandstyle/2021/oct/22/how-to-retrain-your-frazzled-brain-and-find-your-focus-again)):

> Working memory is like a mental whiteboard with disappearing ink.

Sam Harris (he did not say this verbatim, but whatever):

> Death makes a mockery of time spent in negative mental states.

I am not sure where this quote is from, but I like it:

> If you have a one-hour–long meeting with 10 attendees, it is a ten-hour–long meeting.
