Hi! I am Ajay, an aspiring computer scientist. You are in my corner of the internet. Feel free to look around!

Here are some of my most recent writings:

* January 22, 2023 [Data and Intuition](writings/structure_of_data.html)
* October 25, 2022 [The Meaning Affliction](writings/the_meaning_affliction.html)
* September 29, 2022 [Go: Writing Our Very Own WaitGroup](writings/writing_waitgroup.html)
