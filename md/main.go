package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/renderer/html"
)

func wrapBp(s string, level ...int) string {
	relPath := ""
	if len(level) > 0 {
		l := level[0]
		for i := 0; i < l; i++ {
			relPath += "../"
		}
	}
	bp := `<!DOCTYPE html>
<html class="light">
  <head>
    <meta name="robots" content="noindex,nofollow" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> citrus9904 </title>
    <link rel="stylesheet" href="%sstyle.css">
  </head>
  <body>
    <nav id="navbar">
      <div id="navbar-div">
        <a href="%sindex.html">Home</a>
        <a href="%swritings.html">Writings</a>
        <a href="%smeditative_practice.html">Meditative Practice</a>
        <a href="%sinteresting_reads.html">Interesting Reads</a>
        <a href="%sextracts.html">Extracts</a>
        <a href="%scontact.html">Contact</a>
      </div>
      <button id="toggler-button">
	<svg id="sun" xmlns="http://www.w3.org/2000/svg" class="theme-toggler" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z" />
        </svg>
	<svg id="moon" xmlns="http://www.w3.org/2000/svg" class="theme-toggler" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M21.752 15.002A9.718 9.718 0 0118 15.75c-5.385 0-9.75-4.365-9.75-9.75 0-1.33.266-2.597.748-3.752A9.753 9.753 0 003 11.25C3 16.635 7.365 21 12.75 21a9.753 9.753 0 009.002-5.998z" />
        </svg>
      </button>
    </nav>
    </div>
      %s
    <script src="%sindex.js"></script>
  </body>
</html>`
	return fmt.Sprintf(bp, relPath, relPath, relPath, relPath, relPath, relPath, relPath, s, relPath)
}

var generic = wrapBp(`
<div>
  {{.Content}}
</div>`)

var wrtBp = wrapBp(`
<div>
  {{.Content}}
</div>`, 1)

type Content struct {
	Content string
}

func mdToHt(mdPath string, htPath string, t string) {
	tmpl, err := template.New("clever").Parse(t)
	if err != nil {
		panic(err)
	}
	data, err := os.ReadFile(mdPath)
	if err != nil {
		panic(err)
	}
	var b strings.Builder
	md := goldmark.New(
		goldmark.WithRendererOptions(html.WithUnsafe()),
		goldmark.WithExtensions(extension.Table),
	)
	if err = md.Convert(data, &b); err != nil {
		panic(err)
	}
	// Open ht
	f, err := os.Create(htPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	err = tmpl.Execute(f, Content{b.String()})
	if err != nil {
		panic(err)
	}
}

func main() {
	// Writings
	f, err := os.Open("writings")
	defer f.Close()
	if err != nil {
		panic(err)
	}
	sf, err := f.ReadDir(0)
	if err != nil {
		panic(err)
	}
	type indexEl struct {
		t    time.Time
		text string
	}
	indexEls := make([]indexEl, len(sf))
	for _, v := range sf {
		if !v.IsDir() && strings.HasSuffix(v.Name(), ".md") {
			htName := strings.ReplaceAll(v.Name(), ".md", ".html")
			htName = strings.ReplaceAll(htName, "-", "_")
			// Filling writings.md
			b, err := os.ReadFile(fmt.Sprintf("writings/%s", v.Name()))
			if err != nil {
				panic(err)
			}
			sl := strings.Split(string(b), "\n")
			t := strings.ReplaceAll(sl[0], "# ", "") // first line = title
			d := strings.ReplaceAll(sl[2], "**", "") // third line = date
			parsed, err := time.Parse("January 2, 2006", d)
			if err != nil {
				panic(err)
			}
			indexEls = append(indexEls, indexEl{
				parsed,
				fmt.Sprintf("* %s [%s](writings/%s)\n", d, t, htName),
			})

			mdToHt(fmt.Sprintf("./writings/%s", v.Name()), fmt.Sprintf("../writings/%s", htName), wrtBp)
		}
	}
	sort.Slice(indexEls, func(p, q int) bool {
		return indexEls[q].t.Before(indexEls[p].t)
	})
	// Debugging:
	// fmt.Printf("sorted indexEls: %q", indexEls)
	wIndex := ""
	home := `Hi! I am Ajay, an aspiring computer scientist. You are in my corner of the internet. Feel free to look around!

Here are some of my most recent writings:

`
	for i, v := range indexEls {
		wIndex += v.text
		if i < 3 {
			home += v.text
		}
	}
	err = os.WriteFile("writings.md", []byte(wIndex), 0600)
	if err != nil {
		panic(err)
	}
	err = os.WriteFile("index.md", []byte(home), 0600)
	if err != nil {
		panic(err)
	}

	// Meditative practice
	f, err = os.Open("meditative-practice")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	sf, err = f.ReadDir(0)
	if err != nil {
		panic(err)
	}
	indexEls = make([]indexEl, len(sf))
	for _, v := range sf {
		if !v.IsDir() && strings.HasSuffix(v.Name(), ".md") {
			htName := strings.ReplaceAll(v.Name(), ".md", ".html")
			htName = strings.ReplaceAll(htName, "-", "_")
			// Filling writings.md
			b, err := os.ReadFile(fmt.Sprintf("meditative-practice/%s", v.Name()))
			if err != nil {
				panic(err)
			}
			sl := strings.Split(string(b), "\n")
			t := strings.ReplaceAll(sl[0], "# ", "") // first line = title
			d := strings.ReplaceAll(sl[2], "**", "") // third line = date
			parsed, err := time.Parse("January 2, 2006", d)
			if err != nil {
				panic(err)
			}
			indexEls = append(indexEls, indexEl{
				parsed,
				fmt.Sprintf("* %s [%s](meditative-practice/%s)\n", d, t, htName),
			})

			mdToHt(fmt.Sprintf("./meditative-practice/%s", v.Name()), fmt.Sprintf("../meditative-practice/%s", htName), wrtBp)
		}
	}
	sort.Slice(indexEls, func(p, q int) bool {
		return indexEls[q].t.Before(indexEls[p].t)
	})
	data, err := os.ReadFile("meditative-practice-tracker.txt")
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(data), "\n")
	focTotal, vipTotal := 0, 0
	for i, line := range lines {
		if i == 0 || line == "" {
			continue
		}
		row := strings.Split(line, " ")            // date focus Vipassana
		_, err := time.Parse("2006-01-02", row[0]) // Might use this someday
		if err != nil {
			panic(err)
		}
		focus, err := strconv.ParseInt(row[1], 10, 64)
		if err != nil {
			panic(err)
		}
		vip, err := strconv.ParseInt(row[2], 10, 64)
		if err != nil {
			panic(err)
		}
		focTotal += int(focus)
		vipTotal += int(vip)
	}
	medTable := fmt.Sprintf(`
| Meditation Type | Time Spent |
| ---------------------- | --------------- |
| Focus Meditation | %d minutes |
| Vipassana | %d minutes |`, focTotal, vipTotal)

	med := fmt.Sprintf(`Meditation has become an integral part of my life. I feel better about 20 percent better when I regularly meditate. However, the primary reason one must meditate is not so that they feel better. Bear with me, but here is the true reason why one should meditate: to observe the nature of the human mind. When I sit down to meditate, I am always amazed at how much the human mind does, all on its own—thoughts and emotions continually arise, without any seeming participation of myself. Even more bizzarely, they pass away within moments, making me feel a whole different way. Observing this phenomenon is truly powerful. It reveals some experiential truths about the human mind. I find this fascinating.

In this section of my corner of the internet, I intend to share my progress and struggles I face in my meditative practice. I mainly engage in two kinds of meditation: [Vipassana](https://en.wikipedia.org/wiki/Vipassanā) and focus meditation. Both these meditation techniques do not require a belief in one or the other religion—both are non-sectarian, and anybody can take them up.

Vipassana is a technique that encourages clear observation of the human mind—please note that I do not mean this in an abstract sense: Vipassana literally entails clear observation of the human mind. Being able to observe what the mind does, as I have stated before, is powerful, bizarre, and fascinating to the utmost degree. The expert Vipassana practitioner also observes their own conception of the "self", and experiences selflessness (this can also be induced more reliably by psychedelics!), an experience which allows one to become less conscious of themself, and more compassionate and solicitous toward others.

Vipassana must be complemented with focus meditation. Otherwise, the technique is very difficult to practice and master—at least in my experience. Moreover, focus meditation provides a dynamo of tangible real-life benefits, such as, you guessed it, greater focus. I divide focus meditation into two types: experiential and dialogical. In the dialogical method, you focus on a dialogical object of consciousness, such as a thought or a voice in your head (when you repeatedly count a set of numbers, for instance). Experiential focus involves focus on a specific sensation—such as breathing. I have come to prefer focus meditation grounded in experiential focus, because there is already too much dialogical substance in the human mind anyway!

I have Sam Harris to thank for getting me on the meditative path. If you have not already, check out his [Waking Up](https://www.wakingup.com/) app, which has a phenomenal introductory meditation course.

Here is a tracker of the time I spent meditating, since September 26, 2022, for my own reference (you know, the whole 10000 hours deal):

%s

Here are some of my writings describing my journey:

`, medTable)
	for _, v := range indexEls {
		med += v.text
	}
	err = os.WriteFile("meditative-practice.md", []byte(med), 0600)
	if err != nil {
		panic(err)
	}

	// Go through directory
	f, err = os.Open(".")
	defer f.Close() // Is there a problem here with the previous f.Close()?
	if err != nil {
		panic(err)
	}
	sf, err = f.ReadDir(0)
	if err != nil {
		panic(err)
	}
	for _, v := range sf {
		if !v.IsDir() && strings.HasSuffix(v.Name(), ".md") {
			htName := strings.ReplaceAll(v.Name(), ".md", ".html")
			htName = strings.ReplaceAll(htName, "-", "_")
			mdToHt(v.Name(), fmt.Sprintf("../%s", htName), generic)
		}
	}
}
