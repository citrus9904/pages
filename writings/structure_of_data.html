<!DOCTYPE html>
<html class="light">
  <head>
    <meta name="robots" content="noindex,nofollow" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> citrus9904 </title>
    <link rel="stylesheet" href="../style.css">
  </head>
  <body>
    <nav id="navbar">
      <div id="navbar-div">
        <a href="../index.html">Home</a>
        <a href="../writings.html">Writings</a>
        <a href="../meditative_practice.html">Meditative Practice</a>
        <a href="../interesting_reads.html">Interesting Reads</a>
        <a href="../extracts.html">Extracts</a>
        <a href="../contact.html">Contact</a>
      </div>
      <button id="toggler-button">
	<svg id="sun" xmlns="http://www.w3.org/2000/svg" class="theme-toggler" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z" />
        </svg>
	<svg id="moon" xmlns="http://www.w3.org/2000/svg" class="theme-toggler" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M21.752 15.002A9.718 9.718 0 0118 15.75c-5.385 0-9.75-4.365-9.75-9.75 0-1.33.266-2.597.748-3.752A9.753 9.753 0 003 11.25C3 16.635 7.365 21 12.75 21a9.753 9.753 0 009.002-5.998z" />
        </svg>
      </button>
    </nav>
    </div>
      
<div>
  <h1>Data and Intuition</h1>
<p><strong>January 22, 2023</strong></p>
<p>Memory is foundational to computation. Without memory, can you describe to a
computer the computation you want it to perform?</p>
<p>Each of us could organize memory in a different way—for example, I could
represent a number using five physical memory units, and you could decide to do
so using four units. If we all organized memory differently, it would be
difficult for us to share programs. Therefore it is best we agree upon a
standard on how we structure memory. This is one of the reasons why the field
of data structures exists.</p>
<p>In our shared goal of standardization, we could begin by agreeing on how to
represent numbers in memory. That is, how do we structure numbers in memory?
The way we represent numbers in memory is one of the ground-zero data
structures. We could also define other &quot;primitive&quot; data structures such as a
character and a string. (While a string is far from being a &quot;primitive&quot; data
structure, let us abstract away the details for now.)</p>
<p>Now, we have structure to memory. I could tell the computer that, at memory
location 0x02, I have a number, and it will know how exactly this number is
structured. But is manually using memory addresses to keep track of numbers,
and values in general, the best way to write programs? I could have a number at
0x04 that holds my weight, and a number at 0xFF that holds my pet's weight. In
such a program, 0x04 and 0xFF do not lend themselves, at all, to any level of
readability. Other programmers might ask me questions such as &quot;What is the
number you have stored at 0xFF all about? What does it represent?&quot; In addition,
manually dealing with memory locations, is, self-evidently, not suitable for
higher-level programs. This is why we created &quot;variables&quot;, or descriptive names
that reference an address in memory, but that conceal all the low-level memory
dealings under a layer of abstraction. I can now say <code>myWeight = 70</code> and
<code>petWeight = 3</code>, and this makes my program more readable and less prone to
errors. Essentially, we created variables to provide us with a layer of
abstraction over memory.</p>
<p>Going back to numbers, it is important to note that a &quot;number&quot; in itself is not
a data structure--rather, it is an abstract data type, whose operations include
storing and accessing numbers. In any case, we build on these primitive
abstract data types and variables to create ever more abstract types that
unlock higher-order thinking. To illustrate this point, let us examine how we
think about, say, an &quot;article&quot;. When we think about an article, we do not just
think about it in isolation. Rather, its attributes, such as its title, the
date on which it was published, and its author are pulled in into our minds.
That is, an article does not exist in isolation—rather, it is a &quot;collection of
attributes&quot;, and very often, these attributes are primitive abstract data
types. This is why we created &quot;struct&quot;s, which are abstract data types that are
really a collection of several primitive types, all of which serve to describe
a higher-order entity. Here's an example:</p>
<pre><code>struct article {
	title
	author
	nWords
	. . .
}

myArticle = article{title: &quot;Data Structures&quot;, author: &quot;Ajay&quot;, nWords: &quot;1000&quot;}
</code></pre>
<p>Now, when I write a subroutine that computes an article's rating, I could pass
it an &quot;instance&quot; of &quot;article&quot;, which in this case, is <code>myArticle</code>:
<code>computeRating(myArticle)</code>. If I passed to this subroutine the attributes of an
article separately, it wouldn't appeal to our higher-order intuition as much.</p>
<p>Now consider our having to process a hundred articles in our program. We can
create a hundred instances of <code>article</code>, but how do we address each of these
one hundred articles? Only imagine the tedium of creating those many variables!
<code>article1</code>, <code>article2</code>, . . ., <code>article100</code>. Wouldn't this hurt readability of
the program, in addition to the patience of the reader? Wouldn't it be splendid
to store all these <code>article</code>s in a single variable? This is where &quot;arrays&quot; come
in: they allow us to store a collection of values, without our having to name
each one of them: <code>articles = [100]article</code>.  <code>articles</code> allows us to reference
each of its articles by using an index. I could refer to the first article by
using <code>articles[0]</code>, in a zero-indexed data structure, or <code>articles[1]</code> in a
one-indexed data structure. By allowing us to access values by their index,
arrays display their ordered nature. Arrays' inherent order unlocks operations
like sorting values within an array. In our case, we could sort articles by
their ratings, for example, that we obtained from the <code>computeRating</code>
subroutine. We could then publish this sorted array, which now has the best
articles on the top (or the bottom, depending on how it is implemented), on a
website, and people can choose to read the best articles first!</p>
<p>See? As we move to higher and higher levels of thinking, we allow newer
intuitions to form and unlock more possibilities of what computation can do. In
other words, the way we think about data informs the algorithms that operate on
them. Isn't that a thought-provoking idea? It means that if we want to devise a
new way of solving a problem, we should perhaps think of the problem's
substance differently, and move up and down on the scale of abstraction,
unlocking new intuitions along the journey!</p>

</div>
    <script src="../index.js"></script>
  </body>
</html>